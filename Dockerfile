FROM visitee/ruby:2.2.4
MAINTAINER Duc Anh Tran <anhtdse03350@fpt.edu.vn>

# Install PostgreSQL
RUN apt-get update -qq && \
    apt-get install -y --no-install-recommends postgresql postgresql-contrib
    
# Setup base
RUN mkdir -p /visitee
WORKDIR /visitee

# Copy the Gemfile as well as the Gemfile.lock and install 
# the RubyGems. This is a separate step so the dependencies 
# will be cached unless changes to one of those two files 
# are made.
COPY Gemfile Gemfile.lock /visitee/
RUN bundle install

# Copy the rest of the application
COPY . /visitee

# Run the app in production mode by default:
# ENV RACK_ENV=production
# ENV RAILS_ENV=production

# Expose port 3000 to access it from the outside
EXPOSE 3000

# Default cointainer command:
ENTRYPOINT ["bundle","exec"]
CMD ["rails","server","-b","0.0.0.0"]
