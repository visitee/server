class UsergroupsController < ApplicationController
  before_action :set_usergroup, only: [:show, :update, :destroy]

  # GET /usergroups
  # GET /usergroups.json
  def index
    @usergroups = Usergroup.all

    render json: @usergroups
  end

  # GET /usergroups/1
  # GET /usergroups/1.json
  def show
    render json: @usergroup
  end

  # POST /usergroups
  # POST /usergroups.json
  def create
    @usergroup = Usergroup.new(usergroup_params)

    if @usergroup.save
      render json: @usergroup, status: :created, location: @usergroup
    else
      render json: @usergroup.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /usergroups/1
  # PATCH/PUT /usergroups/1.json
  def update
    @usergroup = Usergroup.find(params[:id])

    if @usergroup.update(usergroup_params)
      head :no_content
    else
      render json: @usergroup.errors, status: :unprocessable_entity
    end
  end

  # DELETE /usergroups/1
  # DELETE /usergroups/1.json
  def destroy
    @usergroup.destroy

    head :no_content
  end

  private

    def set_usergroup
      @usergroup = Usergroup.find(params[:id])
    end

    def usergroup_params
      params.require(:usergroup).permit(:name, :description)
    end
end
