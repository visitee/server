class BillingsController < ApplicationController
  before_action :set_billing, only: [:show, :update, :destroy]

  # GET /billings
  # GET /billings.json
  def index
    @billings = Billing.all

    render json: @billings
  end

  # GET /billings/1
  # GET /billings/1.json
  def show
    render json: @billing
  end

  # POST /billings
  # POST /billings.json
  def create
   if(params.has_key?(:sql))
      @billing = Billing.find_by_sql(billing_paramsql[:statement])
      render json: @billing
    else
      @billing = Billing.new(billing_params)
      if @billing.save
        render json: @billing, status: :created, location: @billing
      else
        render json: @billing.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /billings/1
  # PATCH/PUT /billings/1.json
  def update
    @billing = Billing.find(params[:id])

    if @billing.update(billing_params)
      head :no_content
    else
      render json: @billing.errors, status: :unprocessable_entity
    end
  end

  # DELETE /billings/1
  # DELETE /billings/1.json
  def destroy
    @billing.destroy

    head :no_content
  end

  private

    def set_billing
      @billing = Billing.find(params[:id])
    end

    def billing_params
      params.require(:billing).permit( :valuevnd, :valueusd, :status, :overtime, :overvaluevnd, :overvalueusd, :guest_id )
    end
    
     def billing_paramsql
      params.require(:sql).permit(:statement)
    end
end
