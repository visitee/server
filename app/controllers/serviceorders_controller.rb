class ServiceordersController < ApplicationController
  before_action :set_serviceorder, only: [:show, :update, :destroy]

  # GET /serviceorders
  # GET /serviceorders.json
  def index
    @serviceorders = Serviceorder.all

    render json: @serviceorders
  end

  # GET /serviceorders/1
  # GET /serviceorders/1.json
  def show
    render json: @serviceorder
  end

  # POST /serviceorders
  # POST /serviceorders.json
  def create
    @serviceorder = Serviceorder.new(serviceorder_params)

    if @serviceorder.save
      render json: @serviceorder, status: :created, location: @serviceorder
    else
      render json: @serviceorder.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /serviceorders/1
  # PATCH/PUT /serviceorders/1.json
  def update
    @serviceorder = Serviceorder.find(params[:id])

    if @serviceorder.update(serviceorder_params)
      head :no_content
    else
      render json: @serviceorder.errors, status: :unprocessable_entity
    end
  end

  # DELETE /serviceorders/1
  # DELETE /serviceorders/1.json
  def destroy
    @serviceorder.destroy

    head :no_content
  end

  private

    def set_serviceorder
      @serviceorder = Serviceorder.find(params[:id])
    end

    def serviceorder_params
      params.require(:serviceorder).permit(:guest_id, :service_id, :quantity, :description)
    end
end
