class GuestsController < ApplicationController
  before_action :set_guest, only: [:show, :update, :destroy]

  # GET /guests
  # GET /guests.json
  def index
    @guests = Guest.all

    render json: @guests
  end

  # GET /guests/1
  # GET /guests/1.json
  def show
    render json: @guest
  end

  # POST /guests
  # POST /guests.json
  def create
    @guest = Guest.new(guest_params)

    if @guest.save
      render json: @guest, status: :created, location: @guest
    else
      render json: @guest.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /guests/1
  # PATCH/PUT /guests/1.json
  def update
    @guest = Guest.find(params[:id])

    if @guest.update(guest_params)
      head :no_content
    else
      render json: @guest.errors, status: :unprocessable_entity
    end
  end

  # DELETE /guests/1
  # DELETE /guests/1.json
  def destroy
    @guest.destroy

    head :no_content
  end

  private

    def set_guest
      @guest = Guest.find(params[:id])
    end

    def guest_params
      params.require(:guest).permit(:name, :phone, :guesttype, :count, :nationality, :description, :cstidno, :user_id)
    end
end
