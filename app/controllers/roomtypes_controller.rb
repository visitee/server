class RoomtypesController < ApplicationController
  before_action :set_roomtype, only: [:show, :update, :destroy]

  # GET /roomtypes
  # GET /roomtypes.json
  def index
    @roomtypes = Roomtype.all

    render json: @roomtypes
  end

  # GET /roomtypes/1
  # GET /roomtypes/1.json
  def show
    render json: @roomtype
  end

  # POST /roomtypes
  # POST /roomtypes.json
  def create
    @roomtype = Roomtype.new(roomtype_params)

    if @roomtype.save
      render json: @roomtype, status: :created, location: @roomtype
    else
      render json: @roomtype.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /roomtypes/1
  # PATCH/PUT /roomtypes/1.json
  def update
    @roomtype = Roomtype.find(params[:id])

    if @roomtype.update(roomtype_params)
      head :no_content
    else
      render json: @roomtype.errors, status: :unprocessable_entity
    end
  end

  # DELETE /roomtypes/1
  # DELETE /roomtypes/1.json
  def destroy
    @roomtype.destroy

    head :no_content
  end

  private

    def set_roomtype
      @roomtype = Roomtype.find(params[:id])
    end

    def roomtype_params
      params.require(:roomtype).permit(:no, :name, :floor_id, :rfiddevice, :status, :description)
    end
end
