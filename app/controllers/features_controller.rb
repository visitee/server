class FeaturesController < ApplicationController
  before_action :set_feature, only: [:show, :update, :destroy]

  # GET /features
  # GET /features.json
  def index
    @features = Feature.all

    render json: @features
  end

  # GET /features/1
  # GET /features/1.json
  def show
    render json: @feature
  end

  # POST /features
  # POST /features.json
  def create
      @feature = Feature.new(feature_params)

      if @feature.save
        render json: @feature, status: :created, location: @feature
      else
        render json: @feature.errors, status: :unprocessable_entity
      end
  end

  # PATCH/PUT /features/1
  # PATCH/PUT /features/1.json
  def update
    @feature = Feature.find(params[:id])

    if @feature.update(feature_params)
      head :no_content
    else
      render json: @feature.errors, status: :unprocessable_entity
    end
  end

  # DELETE /features/1
  # DELETE /features/1.json
  def destroy
    @feature.destroy

    head :no_content
  end

  private

    def set_feature
      @feature = Feature.find(params[:id])
    end

    def feature_params
      params.require(:feature).permit(:name, :description)
    end

end
