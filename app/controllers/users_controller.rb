class UsersController < ApplicationController
  # GET /users
  # GET /users.json
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    render json: @user
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.find_by_username(user_params_create_new[:username])
    if(@user!=nil)
      render json: @user.errors, status: unprocessable_entity
    end
    @user = User.new(user_params_create_new)
    #logger.debug("#{user_params_create_new}")
    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    head :no_content
  end

  private
    
    def user_params
      params.require(:user).permit(:username, :auth_token)
    end

    def user_params_create_new
      params.require(:user).permit(:username,:password,:password_confirmation,:usergroup_id)
    end
end
