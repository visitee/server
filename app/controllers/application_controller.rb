class ApplicationController < ActionController::API

  include ActionController::HttpAuthentication::Basic::ControllerMethods
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_filter :authenticate_user_from_token, except: [:token]

  def token
    authenticate_with_http_basic do |username, password|
      user = User.find_by(username: username)
      if user && user.authenticate(password)
        permissions=Permission.find_by_sql("Select * from permissions where usergroup_id = "+"#{user.usergroup_id}")
        render json: { AccessToken: user.auth_token , Expires: 1.day.from_now , userId: user.id , userGroupId: user.usergroup_id, CurrentPermissions: permissions }
      else
        render json: { error: 'Incorrect credentials' }, status: 401
      end

    end
  end

  private

  def authenticate_user_from_token
    unless authenticate_with_http_token { |token, options| User.find_by(auth_token: token) }
      logger.debug("#{token}")
      render json: { error: 'Bad Token'}, status: 401
    end
    logger.debug('done')
  end

end
