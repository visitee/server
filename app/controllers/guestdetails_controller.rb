class GuestdetailsController < ApplicationController
  before_action :set_guestdetail, only: [:show, :update, :destroy]

  # GET /guestdetails
  # GET /guestdetails.json
  def index
    @guestdetails = Guestdetail.all

    render json: @guestdetails
  end

  # GET /guestdetails/1
  # GET /guestdetails/1.json
  def show
    render json: @guestdetail
  end

  # POST /guestdetails
  # POST /guestdetails.json
  def create
    @guestdetail = Guestdetail.new(guestdetail_params)

    if @guestdetail.save
      render json: @guestdetail, status: :created, location: @guestdetail
    else
      render json: @guestdetail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /guestdetails/1
  # PATCH/PUT /guestdetails/1.json
  def update
    @guestdetail = Guestdetail.find(params[:id])

    if @guestdetail.update(guestdetail_params)
      head :no_content
    else
      render json: @guestdetail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /guestdetails/1
  # DELETE /guestdetails/1.json
  def destroy
    @guestdetail.destroy

    head :no_content
  end

  private

    def set_guestdetail
      @guestdetail = Guestdetail.find(params[:id])
    end

    def guestdetail_params
      params.require(:guestdetail).permit( :prepaidmoney, :creditcard, :description, :billing_id, :guest_id)
    end
end
