class BookingsController < ApplicationController
  before_action :set_booking, only: [:show, :update, :destroy]

  # GET /bookings
  # GET /bookings.json
  def index
    @bookings = Booking.all

    render json: @bookings
  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
    render json: @booking
  end

  # POST /bookings
  # POST /bookings.json
  def create
    if(params.has_key?(:sql))
      @booking = Booking.find_by_sql(booking_paramsql[:statement])
      render json: @booking
    else
      @booking = Booking.new(booking_params)

      if @booking.save
        render json: @booking, status: :created, location: @booking
      else
        render json: @booking.errors, status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  def update
    @booking = Booking.find(params[:id])

    if @booking.update(booking_params)
      head :no_content
    else
      render json: @booking.errors, status: :unprocessable_entity
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  def destroy
    @booking.destroy

    head :no_content
  end

  private

    def set_booking
      @booking = Booking.find(params[:id])
    end

    def booking_params
      params.require(:booking).permit(  :status, :room_id, :guest_id)
    end

  def booking_paramsql
    params.require(:sql).permit(:statement)
  end
end
