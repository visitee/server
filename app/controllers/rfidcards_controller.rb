class RfidcardsController < ApplicationController
  before_action :set_rfidcard, only: [:show, :update, :destroy]

  # GET /rfidcards
  # GET /rfidcards.json
  def index
    @rfidcards = Rfidcard.all

    render json: @rfidcards
  end

  # GET /rfidcards/1
  # GET /rfidcards/1.json
  def show
    render json: @rfidcard
  end

  # POST /rfidcards
  # POST /rfidcards.json
  def create
    @rfidcard = Rfidcard.find_by_name(rfidcard_params[:rfidno])
    if(@rfidcard==nil)
      @room = Room.find_by_no(rfidcard_params[:roomno])
      @rfidcard = Rfidcard.new(name: rfidcard_params[:rfidno],room_id: @room.id)

      if @rfidcard.save
        render json: @rfidcard, status: :created, location: @rfidcard
      else
        render json: @rfidcard.errors, status: :unprocessable_entity
      end
    else
      render json: @rfidcard.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /rfidcards/1
  # PATCH/PUT /rfidcards/1.json
  def update
    @rfidcard = Rfidcard.find_by_name(params[:id])
    @rfidcardcheckexise = Rfidcard.find_by_name(rfidcard_params[:rfidno])
    if(@rfidcardcheckexise==nil)
      if @rfidcard.update(name: rfidcard_params[:rfidno],description: rfidcard_params[:description])
        head :no_content
      else
        render json: @rfidcard.errors, status: :unprocessable_entity
      end
    else
      render json: @rfidcard.errors, status: :unprocessable_entity
    end

  end

  # DELETE /rfidcards/1
  # DELETE /rfidcards/1.json
  def destroy
    @listcheckinouts = Checkinout.find_by_sql("delete from checkinouts where rfidcard_id = #{@rfidcard.id}")
    @listem = User.find_by_sql("update employees set rfidcard_id = -1 where rfidcard_id = #{@rfidcard.id}")
    @rfidcard.destroy

    head :no_content
  end

  private

    def set_rfidcard
      @rfidcard = Rfidcard.find_by_name(params[:id])
    end

    def rfidcard_params
      params.require(:rfidcard).permit(:rfidno, :roomno,:description)
    end
end
