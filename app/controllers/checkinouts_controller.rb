class CheckinoutsController < ApplicationController
  before_action :set_checkinout, only: [:show, :update, :destroy]

  # GET /checkinouts
  # GET /checkinouts.json
  def index
    @checkinouts = Checkinout.all

    render json: @checkinouts
  end

  # GET /checkinouts/1
  # GET /checkinouts/1.json
  def show
    render json: @checkinout
  end

  # POST /checkinouts
  # POST /checkinouts.json
  def create
    if(params.has_key?(:sql))
      @checkinout = Checkinout.find_by_sql(checkinout_paramsql[:statement])
      render json: @checkinout
    else
        @rfidcard = Rfidcard.find_by_name(checkinout_params[:rfidno])
        @room = Room.find_by_no(checkinout_params[:roomno])
        if(@rfidcard.room_id==@room.id)
          @checkinout = Checkinout.new(rfidcard_id: @rfidcard.id,room_id: @room.id)
          logger.debug("#{request.authorization.split("=")[1]}")
          @user = User.find_by_auth_token(request.authorization.split("=")[1])
          @device = Device.find_by_user_id(@user)
          @device.ipaddress=request.remote_ip
          @device.save
        end
        if @checkinout.save
          #render json: @checkinout, status: :created, location: @checkinout
          @room.status=2
          @room.pin=checkinout_params[:pin]
          @room.save
          a = "update status successful"
          render json:  {message: a }
        else
          render json: @checkinout.errors, status: :unprocessable_entity
        end
      end
  end

  # PATCH/PUT /checkinouts/1
  # PATCH/PUT /checkinouts/1.json
  def update
    @checkinout = Checkinout.find(params[:id])

    if @checkinout.update(checkinout_params)
      head :no_content
    else
      render json: @checkinout.errors, status: :unprocessable_entity
    end
  end

  # DELETE /checkinouts/1
  # DELETE /checkinouts/1.json
  def destroy
    @checkinout.destroy

    head :no_content
  end

  private

    def set_checkinout
      @checkinout = Checkinout.find(params[:id])
    end

    def checkinout_params
      params.require(:checkinout).permit(:rfidno, :roomno, :pin)
    end

    def checkinout_paramsql
      params.require(:sql).permit(:statement)
    end
end
