class FloorsController < ApplicationController
  before_action :set_floor, only: [:show, :update, :destroy]

  # GET /floors
  # GET /floors.json
  def index
    @floors = Floor.all

    render json: @floors
  end

  # GET /floors/1
  # GET /floors/1.json
  def show
    render json: @floor
  end

  # POST /floors
  # POST /floors.json
  def create
    @floor = Floor.new(floor_params)

    if @floor.save
      render json: @floor, status: :created, location: @floor
    else
      render json: @floor.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /floors/1
  # PATCH/PUT /floors/1.json
  def update
    @floor = Floor.find(params[:id])

    if @floor.update(floor_params)
      head :no_content
    else
      render json: @floor.errors, status: :unprocessable_entity
    end
  end

  # DELETE /floors/1
  # DELETE /floors/1.json
  def destroy
    @floor.destroy

    head :no_content
  end

  private

    def set_floor
      @floor = Floor.find(params[:id])
    end

    def floor_params
      params.require(:floor).permit(:name, :description)
    end
end
