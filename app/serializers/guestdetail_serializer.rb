class GuestdetailSerializer < ActiveModel::Serializer
  attributes :id, :billing, :references, :hiredate, :prepaidmoney, :creditcard, :description
  has_one :booking
  has_one :room
  has_one :service
end
