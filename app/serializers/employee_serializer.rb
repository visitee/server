class EmployeeSerializer < ActiveModel::Serializer
  attributes :id, :name, :workingstarttime, :gender, :phone, :description
  has_one :user
  has_one :rfidcard
  has_one :job
  has_one :department
end
