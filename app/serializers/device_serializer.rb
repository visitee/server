class DeviceSerializer < ActiveModel::Serializer
  attributes :id, :name, :ipaddress, :description
  has_one :user
end
