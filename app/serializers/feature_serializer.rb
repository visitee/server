class FeatureSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end
