class CheckinoutSerializer < ActiveModel::Serializer
  attributes :id, :checkintime, :checkouttime
  has_one :user
  has_one :room
end
