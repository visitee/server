class RoomSerializer < ActiveModel::Serializer
  attributes :id, :no, :name, :status, :description
  has_one :floor
  has_one :roomtype
  has_one :device
end
