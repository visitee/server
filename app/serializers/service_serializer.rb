class ServiceSerializer < ActiveModel::Serializer
  attributes :id, :name, :code, :price, :description
end
