class PermissionSerializer < ActiveModel::Serializer
  attributes :id, :description
  has_one :usergroup
  has_one :feature
end
