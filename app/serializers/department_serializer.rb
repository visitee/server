class DepartmentSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :totalemployees
end
