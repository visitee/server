class RfidcardSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
  has_one :room
end
