class BillingSerializer < ActiveModel::Serializer
  attributes :id, :billdate, :valuevnd, :valueusd, :status, :overtime, :overvaluevnd, :overvalueusd
  has_one :service
end
