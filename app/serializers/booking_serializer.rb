class BookingSerializer < ActiveModel::Serializer
  attributes :id, :bookingtime
  has_one :service
  has_one :room
  has_one :renter
end
