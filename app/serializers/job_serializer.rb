class JobSerializer < ActiveModel::Serializer
  attributes :id, :jobtitle, :joblevel, :description
end
