class GuestSerializer < ActiveModel::Serializer
  attributes :id, :name, :phone, :guesttype, :count, :nationality, :description, :cstidno
  has_one :user
end
