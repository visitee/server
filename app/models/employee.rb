class Employee < ActiveRecord::Base
  belongs_to :user
  belongs_to :rfidcard
  belongs_to :job
  belongs_to :department
end
