class Room < ActiveRecord::Base
  belongs_to :floor
  belongs_to :roomtype
  belongs_to :device
end
