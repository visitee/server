class User < ActiveRecord::Base
  has_secure_password
  before_create -> { self.auth_token = SecureRandom.hex }
  before_update -> { self.auth_token = SecureRandom.hex }
  before_save -> { self.auth_token = SecureRandom.hex }
  belongs_to :usergroup
end
