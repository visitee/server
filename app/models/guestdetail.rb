class Guestdetail < ActiveRecord::Base
  belongs_to :room
  belongs_to :billing
  belongs_to :guest
end
