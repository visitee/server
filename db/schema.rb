# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160501192852) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "billings", force: :cascade do |t|
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.float    "valuevnd"
    t.float    "valueusd"
    t.integer  "status"
    t.integer  "overtime"
    t.float    "overvaluevnd"
    t.float    "overvalueusd"
    t.integer  "guest_id"
  end

  add_index "billings", ["guest_id"], name: "index_billings_on_guest_id", using: :btree

  create_table "bookings", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "status"
    t.integer  "room_id"
    t.integer  "guest_id"
  end

  add_index "bookings", ["guest_id"], name: "index_bookings_on_guest_id", using: :btree
  add_index "bookings", ["room_id"], name: "index_bookings_on_room_id", using: :btree

  create_table "checkinouts", force: :cascade do |t|
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "rfidcard_id"
    t.integer  "room_id"
  end

  add_index "checkinouts", ["rfidcard_id"], name: "index_checkinouts_on_rfidcard_id", using: :btree
  add_index "checkinouts", ["room_id"], name: "index_checkinouts_on_room_id", using: :btree

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "totalemployees"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "devices", force: :cascade do |t|
    t.string   "name"
    t.string   "ipaddress"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "employees", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "gender"
    t.string   "phone"
    t.integer  "rfidcard_id"
    t.text     "description"
    t.integer  "job_id"
    t.integer  "department_id"
  end

  add_index "employees", ["department_id"], name: "index_employees_on_department_id", using: :btree
  add_index "employees", ["job_id"], name: "index_employees_on_job_id", using: :btree
  add_index "employees", ["rfidcard_id"], name: "index_employees_on_rfidcard_id", using: :btree
  add_index "employees", ["user_id"], name: "index_employees_on_user_id", using: :btree

  create_table "features", force: :cascade do |t|
    t.string   "namevn"
    t.string   "nameen"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "floors", force: :cascade do |t|
    t.text     "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "guestdetails", force: :cascade do |t|
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.float    "prepaidmoney"
    t.string   "creditcard"
    t.integer  "guest_id"
    t.text     "description"
    t.integer  "billing_id"
  end

  add_index "guestdetails", ["billing_id"], name: "index_guestdetails_on_billing_id", using: :btree
  add_index "guestdetails", ["guest_id"], name: "index_guestdetails_on_guest_id", using: :btree

  create_table "guests", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.integer  "guesttype"
    t.integer  "count"
    t.string   "nationality"
    t.text     "description"
    t.string   "cstidno"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "guests", ["user_id"], name: "index_guests_on_user_id", using: :btree

  create_table "jobs", force: :cascade do |t|
    t.string   "title"
    t.integer  "level"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "usergroup_id"
    t.integer  "feature_id"
    t.text     "description"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "permissions", ["feature_id"], name: "index_permissions_on_feature_id", using: :btree
  add_index "permissions", ["usergroup_id"], name: "index_permissions_on_usergroup_id", using: :btree

  create_table "rfidcards", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "room_id"
  end

  add_index "rfidcards", ["room_id"], name: "index_rfidcards_on_room_id", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.string   "no"
    t.string   "name"
    t.integer  "status"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "floor_id"
    t.integer  "roomtype_id"
    t.integer  "device_id"
    t.float    "pin"
  end

  add_index "rooms", ["device_id"], name: "index_rooms_on_device_id", using: :btree
  add_index "rooms", ["floor_id"], name: "index_rooms_on_floor_id", using: :btree
  add_index "rooms", ["roomtype_id"], name: "index_rooms_on_roomtype_id", using: :btree

  create_table "roomtypes", force: :cascade do |t|
    t.string   "name",         null: false
    t.string   "namevn"
    t.text     "description"
    t.integer  "base_rate",    null: false
    t.integer  "extra_rate",   null: false
    t.integer  "fullday_rate", null: false
    t.integer  "night_rate",   null: false
    t.integer  "special_rate"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "serviceorders", force: :cascade do |t|
    t.integer  "quantity"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "guest_id"
    t.integer  "service_id"
  end

  add_index "serviceorders", ["guest_id"], name: "index_serviceorders_on_guest_id", using: :btree
  add_index "serviceorders", ["service_id"], name: "index_serviceorders_on_service_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.float    "price"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "usergroups", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "username"
    t.string   "auth_token"
    t.string   "password"
    t.string   "password_confirmation"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "usergroup_id"
  end

  add_index "users", ["usergroup_id"], name: "index_users_on_usergroup_id", using: :btree

  add_foreign_key "billings", "guests"
  add_foreign_key "bookings", "guests"
  add_foreign_key "bookings", "rooms"
  add_foreign_key "checkinouts", "rfidcards"
  add_foreign_key "checkinouts", "rooms"
  add_foreign_key "devices", "users"
  add_foreign_key "employees", "departments"
  add_foreign_key "employees", "jobs"
  add_foreign_key "employees", "rfidcards"
  add_foreign_key "employees", "users"
  add_foreign_key "guestdetails", "billings"
  add_foreign_key "guests", "users"
  add_foreign_key "permissions", "features"
  add_foreign_key "permissions", "usergroups"
  add_foreign_key "rfidcards", "rooms"
  add_foreign_key "rooms", "devices"
  add_foreign_key "rooms", "floors"
  add_foreign_key "rooms", "roomtypes"
  add_foreign_key "serviceorders", "guests"
  add_foreign_key "serviceorders", "services"
  add_foreign_key "users", "usergroups"
end
