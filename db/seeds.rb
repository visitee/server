gr1 = Usergroup.create(name: 'admin', description: 'admin')
gr2 = Usergroup.create(name: 'guest', description: 'guest')
gr3 = Usergroup.create(name: 'staff', description: 'staff')
gr4 = Usergroup.create(name: 'device', description: 'device')

admin1 = gr1.user.create(username: 'admin1', password: 'password1', password_confirmation: 'password1')
admin2 = gr1.user.create(username: 'admin2', password: 'password2', password_confirmation: 'password2')
admin3 = gr1.user.create(username: 'admin3', password: 'password3', password_confirmation: 'password3')
admin4 = gr1.user.create(username: 'admin4', password: 'password4', password_confirmation: 'password4')
admin5 = gr1.user.create(username: 'admin5', password: 'password5', password_confirmation: 'password5')
admin6 = gr1.user.create(username: 'admin6', password: 'password6', password_confirmation: 'password6')

guest1 = gr2.user.create(username: 'guest1', password: 'password1', password_confirmation: 'password1')
guest2 = gr2.user.create(username: 'guest2', password: 'password2', password_confirmation: 'password2')
guest3 = gr2.user.create(username: 'guest3', password: 'password3', password_confirmation: 'password3')
guest4 = gr2.user.create(username: 'guest4', password: 'password4', password_confirmation: 'password4')
guest5 = gr2.user.create(username: 'guest5', password: 'password5', password_confirmation: 'password5')
guest6 = gr2.user.create(username: 'guest6', password: 'password6', password_confirmation: 'password6')

staff1 = gr3.user.create(username: 'staff1', password: 'password1', password_confirmation: 'password1')
staff2 = gr3.user.create(username: 'staff2', password: 'password2', password_confirmation: 'password2')
staff3 = gr3.user.create(username: 'staff3', password: 'password3', password_confirmation: 'password3')
staff4 = gr3.user.create(username: 'staff4', password: 'password4', password_confirmation: 'password4')
staff5 = gr3.user.create(username: 'staff5', password: 'password5', password_confirmation: 'password5')
staff6 = gr3.user.create(username: 'staff6', password: 'password6', password_confirmation: 'password6')

deviceusr1 = gr4.user.create(username: 'device1', password: 'password1', password_confirmation: 'password1')
deviceusr2 = gr4.user.create(username: 'device2', password: 'password2', password_confirmation: 'password2')
deviceusr3 = gr4.user.create(username: 'device3', password: 'password3', password_confirmation: 'password3')
deviceusr4 = gr4.user.create(username: 'device4', password: 'password4', password_confirmation: 'password4')
deviceusr5 = gr4.user.create(username: 'device5', password: 'password5', password_confirmation: 'password5')
deviceusr6 = gr4.user.create(username: 'device6', password: 'password6', password_confirmation: 'password6')

f1 = Feature.create(namevn: 'Quản lý phòng',        nameen: 'Room Management')
f2 = Feature.create(namevn: 'Đăng ký thuê phòng',   nameen: 'Room Registration')
f3 = Feature.create(namevn: 'Xác nhận trả phòng',    nameen: 'Room Checkout Confirmation')
f4 = Feature.create(namevn: 'Mục Nhân viên Quản lý',   nameen: 'Staff Management')
f5 = Feature.create(namevn: 'Mục Quản trị Viên Quản lý',           nameen: 'Admin Management')
f6 = Feature.create(namevn: 'Báo cáo Tài chính', nameen: 'Financial Report')
f7 = Feature.create(namevn: 'Báo cáo Vào Ra', nameen: 'CheckinOut Report')
f8 = Feature.create(namevn: 'Trợ giúp',    nameen: 'Help')
f9 = Feature.create(namevn: 'Thông tin',      nameen: 'About') 

Permission.create(usergroup:gr1,feature:f1)
Permission.create(usergroup:gr1,feature:f2)
Permission.create(usergroup:gr1,feature:f3)
Permission.create(usergroup:gr1,feature:f4)
Permission.create(usergroup:gr1,feature:f5)
Permission.create(usergroup:gr1,feature:f6)
Permission.create(usergroup:gr1,feature:f7)
Permission.create(usergroup:gr1,feature:f8)
Permission.create(usergroup:gr1,feature:f9)  

Permission.create(usergroup:gr2,feature:f1)
Permission.create(usergroup:gr2,feature:f2)
Permission.create(usergroup:gr2,feature:f3)
Permission.create(usergroup:gr2,feature:f4)
Permission.create(usergroup:gr2,feature:f5)
Permission.create(usergroup:gr2,feature:f6)
Permission.create(usergroup:gr2,feature:f7)
Permission.create(usergroup:gr2,feature:f8) 
Permission.create(usergroup:gr2,feature:f9) 

Permission.create(usergroup:gr3,feature:f1)
Permission.create(usergroup:gr3,feature:f2)
Permission.create(usergroup:gr3,feature:f3)
Permission.create(usergroup:gr3,feature:f4)
Permission.create(usergroup:gr3,feature:f5)
Permission.create(usergroup:gr3,feature:f6)
Permission.create(usergroup:gr3,feature:f7)
Permission.create(usergroup:gr3,feature:f8) 
Permission.create(usergroup:gr3,feature:f9) 

Permission.create(usergroup:gr4,feature:f1)
Permission.create(usergroup:gr4,feature:f2)
Permission.create(usergroup:gr4,feature:f3)
Permission.create(usergroup:gr4,feature:f4)
Permission.create(usergroup:gr4,feature:f5)
Permission.create(usergroup:gr4,feature:f6)
Permission.create(usergroup:gr4,feature:f7)
Permission.create(usergroup:gr4,feature:f8) 
Permission.create(usergroup:gr4,feature:f9) 

floor1 = Floor.create(name: 'Tầng 1', description: 'Tầng 1')
floor2 = Floor.create(name: 'Tầng 2', description: 'Tầng 2')
floor3 = Floor.create(name: 'Tầng 3', description: 'Tầng 3')
floor4 = Floor.create(name: 'Tầng 4', description: 'Tầng 4')

type1 = Roomtype.create(name: 'Normal', namevn: 'Phòng đơn', base_rate: 60000,  extra_rate: 10000, fullday_rate: 200000, night_rate: 150000, special_rate: 0)
type2 = Roomtype.create(name: 'Double', namevn: 'Phòng đôi', base_rate: 90000,  extra_rate: 10000, fullday_rate: 220000, night_rate: 180000, special_rate: 0)
type3 = Roomtype.create(name: 'VIP',    namevn: 'Phòng VIP', base_rate: 120000, extra_rate: 30000, fullday_rate: 400000, night_rate: 250000, special_rate: 0)

device1 = Device.create(name: 'receiver1', description: 'Tầng 1', user: deviceusr1)
device2 = Device.create(name: 'receiver2', description: 'Tầng 2', user: deviceusr2)
device3 = Device.create(name: 'receiver3', description: 'Tầng 3', user: deviceusr3)
device4 = Device.create(name: 'receiver4', description: 'Tầng 4', user: deviceusr4)

room1 = Room.create(no: '101', name: 'Phòng 101',  status: '1', description: 'Phòng 101', floor: floor1, roomtype: type1, device: device1)
room2 = Room.create(no: '102', name: 'Phòng 102',  status: '1', description: 'Phòng 102', floor: floor1, roomtype: type1, device: device1)
room3 = Room.create(no: '103', name: 'Phòng 103',  status: '1', description: 'Phòng 103', floor: floor1, roomtype: type2, device: device1)
room4 = Room.create(no: '201', name: 'Phòng 201',  status: '1', description: 'Phòng 201', floor: floor2, roomtype: type1, device: device2)
room5 = Room.create(no: '202', name: 'Phòng 202',  status: '1', description: 'Phòng 202', floor: floor2, roomtype: type2, device: device2)
room6 = Room.create(no: '203', name: 'Phòng 203',  status: '1', description: 'Phòng 203', floor: floor2, roomtype: type3, device: device2)
room7 = Room.create(no: '301', name: 'Phòng 301',  status: '1', description: 'Phòng 301', floor: floor3, roomtype: type1, device: device3)
room8 = Room.create(no: '302', name: 'Phòng 302',  status: '1', description: 'Phòng 302', floor: floor3, roomtype: type1, device: device3)
room9 = Room.create(no: '303', name: 'Phòng 303',  status: '1', description: 'Phòng 303', floor: floor3, roomtype: type2, device: device3)
room10 = Room.create(no: '401', name: 'Phòng 401',  status: '1', description: 'Phòng 401', floor: floor4, roomtype: type1, device: device4)
room11 = Room.create(no: '402', name: 'Phòng 402',  status: '1', description: 'Phòng 402', floor: floor4, roomtype: type2, device: device4)
room12 = Room.create(no: '403', name: 'Phòng 403',  status: '1', description: 'Phòng 403', floor: floor4, roomtype: type3, device: device4)

rfidcard1 = Rfidcard.create(name: '20248245233240', description: 'thẻ 1', room: room1)
rfidcard2 = Rfidcard.create(name: '8914011313333',  description: 'thẻ 2', room: room1)

job1 = Job.create(title: 'Manager', level: 1)
job2 = Job.create(title: 'Receptionist', level: 2)
job3 = Job.create(title: 'Cleaning', level: 3)

dep1 = Department.create(name: 'Lễ Tân',totalemployees: '2')
dep2 = Department.create(name: 'Phục vụ phòng',totalemployees: '2')
dep3 = Department.create(name: 'Bồi bàn',totalemployees: '2')

employee1 = Employee.create(user: staff1, name:'Toan',  gender:'Male', phone: '01666054540', rfidcard: rfidcard1 , job: job1, department: dep1)
employee2 = Employee.create(user: staff2, name:'Thanh', gender:'Male', phone: '1234567780',  rfidcard: rfidcard2 , job: job2, department: dep1)
employee3 = Employee.create(user: staff3, name:'Dat',   gender:'Male', phone: '01666054540', rfidcard: rfidcard1 , job: job3, department: dep2)
employee4 = Employee.create(user: staff4, name:'DucAnh',gender:'Male', phone: '1234567780',  rfidcard: rfidcard2 , job: job1, department: dep2)
employee5 = Employee.create(user: staff5, name:'Toan',  gender:'Male', phone: '01666054540', rfidcard: rfidcard1 , job: job3, department: dep3)
employee6 = Employee.create(user: staff6, name:'Thanh', gender:'Male', phone: '1234567780',  rfidcard: rfidcard2 , job: job2, department: dep3)


guest1 = Guest.create(user: guest1, name: 'Nguyen Van A', phone: '0987654321', guesttype: '1', count: '1', cstidno: '063374000', nationality: 'VietNam', description: 'none')
guest2 = Guest.create(user: guest2, name: 'Nguyen Van B', phone: '0987654321', guesttype: '1', count: '2', cstidno: '063374000', nationality: 'VietNam', description: 'none')
guest3 = Guest.create(user: guest3, name: 'Nguyen Van C', phone: '0987654321', guesttype: '1', count: '1', cstidno: '063374000', nationality: 'VietNam', description: 'none')
guest4 = Guest.create(user: guest4, name: 'Nguyen Van D', phone: '0987654321', guesttype: '1', count: '3', cstidno: '063374000', nationality: 'VietNam', description: 'none')
guest5 = Guest.create(user: guest5, name: 'Nguyen Van E', phone: '0987654321', guesttype: '1', count: '4', cstidno: '063374000', nationality: 'VietNam', description: 'none')
guest6 = Guest.create(user: guest6, name: 'Nguyen Van F', phone: '0987654321', guesttype: '1', count: '2', cstidno: '063374000', nationality: 'VietNam', description: 'none')

for i in 0..500
  billing=Billing.create( valuevnd: rand*1000000, valueusd: rand*1000, status: '1', overtime: '1', overvaluevnd: '100', overvalueusd: '1',guest: guest1)
  billing.update_attribute :created_at, (rand*100).days.ago
end
billing1=Billing.create( valuevnd: '10000000', valueusd: '2000', status: '1', overtime: '1', overvaluevnd: '100', overvalueusd: '1',guest: guest1)
billing1.update_attribute :created_at, (rand*100).days.ago
billing2=Billing.create( valuevnd: '10000000', valueusd: '2000', status: '1', overtime: '1', overvaluevnd: '100', overvalueusd: '1',guest: guest2)
billing2.update_attribute :created_at, (rand*100).days.ago
billing3=Billing.create( valuevnd: '10000000', valueusd: '2000', status: '2', overtime: '1', overvaluevnd: '100', overvalueusd: '1',guest: guest3)
billing3.update_attribute :created_at, (rand*100).days.ago
billing4=Billing.create( valuevnd: '10000000', valueusd: '2000', status: '2', overtime: '1', overvaluevnd: '100', overvalueusd: '1',guest: guest4)
billing4.update_attribute :created_at, (rand*100).days.ago
billing5=Billing.create(valuevnd: '10000000', valueusd: '2000', status: '1', overtime: '1', overvaluevnd: '100', overvalueusd: '1', guest: guest5)
billing5.update_attribute :created_at, (rand*100).days.ago
billing6=Billing.create(valuevnd: '10000000', valueusd: '2000', status: '1', overtime: '1', overvaluevnd: '100', overvalueusd: '1', guest: guest6)
billing6.update_attribute :created_at, (rand*100).days.ago

for i in 0..75
booking1 = Booking.create( room: room1, guest: guest1, status: '1')
booking1.update_attribute :created_at, (rand*300).days.ago
booking1.update_attribute :updated_at, booking1.created_at + (rand*3).days
booking2 = Booking.create( room: room2, guest: guest2, status: '1')
booking2.update_attribute :created_at, (rand*300).days.ago
booking2.update_attribute :updated_at, booking2.created_at + (rand*3).days
booking3 = Booking.create( room: room3, guest: guest3, status: '1')
booking3.update_attribute :created_at, (rand*300).days.ago
booking3.update_attribute :updated_at, booking3.created_at + (rand*3).days
booking4 = Booking.create( room: room4,guest: guest1, status: '1')
booking4.update_attribute :created_at, (rand*300).days.ago
booking4.update_attribute :updated_at, booking4.created_at + (rand*3).days
booking5 = Booking.create( room: room5, guest: guest4, status: '1')
booking5.update_attribute :created_at, (rand*300).days.ago
booking5.update_attribute :updated_at, booking5.created_at + (rand*3).days
booking6 = Booking.create( room: room6, guest: guest2, status: '1')
booking6.update_attribute :created_at, (rand*300).days.ago
booking6.update_attribute :updated_at, booking6.created_at + (rand*3).days
booking7 = Booking.create( room: room7, guest: guest1, status: '1')
booking7.update_attribute :created_at, (rand*300).days.ago
booking7.update_attribute :updated_at, booking1.created_at + (rand*3).days
booking8 = Booking.create( room: room8, guest: guest2, status: '1')
booking8.update_attribute :created_at, (rand*300).days.ago
booking8.update_attribute :updated_at, booking2.created_at + (rand*3).days
booking9 = Booking.create( room: room9, guest: guest3, status: '1')
booking9.update_attribute :created_at, (rand*300).days.ago
booking9.update_attribute :updated_at, booking3.created_at + (rand*3).days
booking10 = Booking.create( room: room10,guest: guest1, status: '1')
booking10.update_attribute :created_at, (rand*300).days.ago
booking10.update_attribute :updated_at, booking4.created_at + (rand*3).days
booking11 = Booking.create( room: room11, guest: guest4, status: '1')
booking11.update_attribute :created_at, (rand*300).days.ago
booking11.update_attribute :updated_at, booking5.created_at + (rand*3).days
booking12 = Booking.create( room: room12, guest: guest2, status: '1')
booking12.update_attribute :created_at, (rand*300).days.ago
booking12.update_attribute :updated_at, booking6.created_at + (rand*3).days
end

service1 = Service.create( name: 'Giặt Là', code: 'GL', price: '30000',description: 'Giặt bằng máy giặt')
service2 = Service.create( name: 'Ăn Sáng', code: 'AS', price: '25000',description: 'none')
service3 = Service.create( name: 'Ăn Trưa', code: 'ATR', price: '40000', description: 'none')
service4 = Service.create( name: 'Ăn Tối', code: 'ATO', price: '40000', description: 'none')

serviceorder1 = Serviceorder.create(guest:guest1,service: service1,quantity: '1', description: 'Giao trước 10h sáng')
serviceorder2 = Serviceorder.create(guest:guest1,service: service2,quantity: '1', description: 'Phở bò')
serviceorder3 = Serviceorder.create(guest:guest1,service: service3,quantity: '1', description: 'Cơm rang')
serviceorder4 = Serviceorder.create(guest:guest1,service: service4,quantity: '1', description: 'Cơm')

guestdetail1 = Guestdetail.create(billing: billing1, prepaidmoney: '1000000',creditcard: '123456789',guest: guest1)
guestdetail2 = Guestdetail.create(billing: billing2, prepaidmoney: '1000000',creditcard: '987312312',guest: guest2)
guestdetail3 = Guestdetail.create(billing: billing3, prepaidmoney: '1000000',creditcard: '987322312',guest: guest3)
guestdetail4 = Guestdetail.create(billing: billing4, prepaidmoney: '1000000',creditcard: '437312312',guest: guest4)

for i in 0..100
  Room.all.each do |room|
    checkinout = Checkinout.create(rfidcard: rfidcard1,room: room)
    checkinout.update_attribute :created_at, (rand*100).days.ago
  end
end
