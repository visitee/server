class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.timestamps :workingstarttime
      t.string :gender
      t.string :phone
      t.references :rfidcard, index: true, foreign_key: true
      t.text :description
      t.references :job, index: true, foreign_key: true
      t.references :department, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
