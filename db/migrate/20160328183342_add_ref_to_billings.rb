class AddRefToBillings < ActiveRecord::Migration
  def change
    add_reference :billings, :guest, index: true, foreign_key: true
  end
end
