class AddRefToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :usergroup, index: true, foreign_key: true
  end
end
