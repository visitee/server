class AddRefToGuestdetails < ActiveRecord::Migration
  def change
      add_reference :guestdetails, :billing, index: true, foreign_key: true
  end
end
