class CreateCheckinouts < ActiveRecord::Migration
  def change
    create_table :checkinouts do |t|
      t.timestamps :checkintime
      t.timestamps :checkouttime

      t.timestamps null: false
    end
  end
end
