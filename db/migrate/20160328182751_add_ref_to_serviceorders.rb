class AddRefToServiceorders < ActiveRecord::Migration
  def change
    add_reference :serviceorders,:guest, index: true, foreign_key: true
    add_reference :serviceorders,:service, index: true, foreign_key: true
  end
end
