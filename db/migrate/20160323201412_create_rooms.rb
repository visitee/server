class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :no
      t.string :name
      t.integer :status
      t.text :description
      t.timestamps null: false
    end
  end
end
