class AddRefToRfidcards < ActiveRecord::Migration
  def change
    add_reference :rfidcards, :room, index: true, foreign_key: true
  end
end
