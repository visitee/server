class CreateRoomtypes < ActiveRecord::Migration
  def change
    create_table :roomtypes do |t|
      t.string :name, null: false
      t.string :namevn
      t.text :description
      t.integer :base_rate, null: false
      t.integer :extra_rate, null: false
      t.integer :fullday_rate, null: false
      t.integer :night_rate, null: false
      t.integer :special_rate

      t.timestamps null: false
    end
  end
end
