class CreateServiceorders < ActiveRecord::Migration
  def change
    create_table :serviceorders do |t|
      t.integer :quantity
      t.text :description

      t.timestamps null: false
    end
  end
end
