class AddRefToCheckinouts < ActiveRecord::Migration
  def change
    add_reference :checkinouts, :rfidcard, index: true, foreign_key: true
    add_reference :checkinouts, :room, index: true, foreign_key: true
  end
end
