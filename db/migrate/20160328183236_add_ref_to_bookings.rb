class AddRefToBookings < ActiveRecord::Migration
  def change
    add_reference :bookings, :room, index: true, foreign_key: true
    add_reference :bookings, :guest, index: true, foreign_key: true
  end
end
