class CreateGuestdetails < ActiveRecord::Migration
  def change
    create_table :guestdetails do |t|
      t.timestamps :hiredate
      t.float :prepaidmoney
      t.string :creditcard
      t.belongs_to :guest, index: true
      t.text :description
      t.timestamps null: false
    end
  end
end
