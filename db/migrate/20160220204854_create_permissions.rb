class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.references :usergroup, index: true, foreign_key: true
      t.references :feature, index: true, foreign_key: true
      t.text :description

      t.timestamps null: false
    end
  end
end
