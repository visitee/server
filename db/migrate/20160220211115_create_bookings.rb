class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.timestamps :bookingtime
      t.integer :status
      t.timestamps null: false
    end
  end
end
