class AddRefToRooms < ActiveRecord::Migration
  def change
      add_reference :rooms, :floor, index: true, foreign_key: true
      add_reference :rooms, :roomtype, index: true, foreign_key: true
      add_reference :rooms, :device, index: true, foreign_key: true
  end
end
