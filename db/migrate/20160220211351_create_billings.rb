class CreateBillings < ActiveRecord::Migration
  def change
    create_table :billings do |t|
      t.timestamps :billdate
      t.float :valuevnd
      t.float :valueusd
      t.integer :status
      t.integer :overtime
      t.float :overvaluevnd
      t.float :overvalueusd
      t.timestamps null: false
    end
  end
end
