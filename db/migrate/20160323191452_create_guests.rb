class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :name
      t.string :phone
      t.integer :guesttype
      t.integer :count
      t.string :nationality
      t.text :description
      t.string :cstidno
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
