require 'test_helper'

class BillingsControllerTest < ActionController::TestCase
  setup do
    @billing = billings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:billings)
  end

  test "should create billing" do
    assert_difference('Billing.count') do
      post :create, billing: { billdate: @billing.billdate, overtime: @billing.overtime, overvalueusd: @billing.overvalueusd, overvaluevnd: @billing.overvaluevnd, service_id: @billing.service_id, status: @billing.status, valueusd: @billing.valueusd, valuevnd: @billing.valuevnd }
    end

    assert_response 201
  end

  test "should show billing" do
    get :show, id: @billing
    assert_response :success
  end

  test "should update billing" do
    put :update, id: @billing, billing: { billdate: @billing.billdate, overtime: @billing.overtime, overvalueusd: @billing.overvalueusd, overvaluevnd: @billing.overvaluevnd, service_id: @billing.service_id, status: @billing.status, valueusd: @billing.valueusd, valuevnd: @billing.valuevnd }
    assert_response 204
  end

  test "should destroy billing" do
    assert_difference('Billing.count', -1) do
      delete :destroy, id: @billing
    end

    assert_response 204
  end
end
