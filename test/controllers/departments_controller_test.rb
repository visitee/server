require 'test_helper'

class DepartmentsControllerTest < ActionController::TestCase
  setup do
    @department = departments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:departments)
  end

  test "should create department" do
    assert_difference('Department.count') do
      post :create, department: { description: @department.description, name: @department.name, totalemployees: @department.totalemployees }
    end

    assert_response 201
  end

  test "should show department" do
    get :show, id: @department
    assert_response :success
  end

  test "should update department" do
    put :update, id: @department, department: { description: @department.description, name: @department.name, totalemployees: @department.totalemployees }
    assert_response 204
  end

  test "should destroy department" do
    assert_difference('Department.count', -1) do
      delete :destroy, id: @department
    end

    assert_response 204
  end
end
