require 'test_helper'

class EmployeesControllerTest < ActionController::TestCase
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:employees)
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post :create, employee: { department_id: @employee.department_id, description: @employee.description, gender: @employee.gender, job_id: @employee.job_id, name: @employee.name, phone: @employee.phone, rfidcard_id: @employee.rfidcard_id, user_id: @employee.user_id, workingstarttime: @employee.workingstarttime }
    end

    assert_response 201
  end

  test "should show employee" do
    get :show, id: @employee
    assert_response :success
  end

  test "should update employee" do
    put :update, id: @employee, employee: { department_id: @employee.department_id, description: @employee.description, gender: @employee.gender, job_id: @employee.job_id, name: @employee.name, phone: @employee.phone, rfidcard_id: @employee.rfidcard_id, user_id: @employee.user_id, workingstarttime: @employee.workingstarttime }
    assert_response 204
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete :destroy, id: @employee
    end

    assert_response 204
  end
end
