require 'test_helper'

class GuestdetailsControllerTest < ActionController::TestCase
  setup do
    @guestdetail = guestdetails(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:guestdetails)
  end

  test "should create guestdetail" do
    assert_difference('Guestdetail.count') do
      post :create, guestdetail: {  }
    end

    assert_response 201
  end

  test "should show guestdetail" do
    get :show, id: @guestdetail
    assert_response :success
  end

  test "should update guestdetail" do
    put :update, id: @guestdetail, guestdetail: {  }
    assert_response 204
  end

  test "should destroy guestdetail" do
    assert_difference('Guestdetail.count', -1) do
      delete :destroy, id: @guestdetail
    end

    assert_response 204
  end
end
