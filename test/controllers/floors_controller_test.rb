require 'test_helper'

class FloorsControllerTest < ActionController::TestCase
  setup do
    @floor = floors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:floors)
  end

  test "should create floor" do
    assert_difference('Floor.count') do
      post :create, floor: { : @floor., description: @floor.description, name: @floor.name, room_id: @floor.room_id }
    end

    assert_response 201
  end

  test "should show floor" do
    get :show, id: @floor
    assert_response :success
  end

  test "should update floor" do
    put :update, id: @floor, floor: { : @floor., description: @floor.description, name: @floor.name, room_id: @floor.room_id }
    assert_response 204
  end

  test "should destroy floor" do
    assert_difference('Floor.count', -1) do
      delete :destroy, id: @floor
    end

    assert_response 204
  end
end
