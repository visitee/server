require 'test_helper'

class UsergroupsControllerTest < ActionController::TestCase
  setup do
    @usergroup = usergroups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:usergroups)
  end

  test "should create usergroup" do
    assert_difference('Usergroup.count') do
      post :create, usergroup: { description: @usergroup.description, name: @usergroup.name }
    end

    assert_response 201
  end

  test "should show usergroup" do
    get :show, id: @usergroup
    assert_response :success
  end

  test "should update usergroup" do
    put :update, id: @usergroup, usergroup: { description: @usergroup.description, name: @usergroup.name }
    assert_response 204
  end

  test "should destroy usergroup" do
    assert_difference('Usergroup.count', -1) do
      delete :destroy, id: @usergroup
    end

    assert_response 204
  end
end
