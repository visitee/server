require 'test_helper'

class FeaturesControllerTest < ActionController::TestCase
  setup do
    @feature = features(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:features)
  end

  test "should create feature" do
    assert_difference('Feature.count') do
      post :create, feature: { description: @feature.description, name: @feature.name }
    end

    assert_response 201
  end

  test "should show feature" do
    get :show, id: @feature
    assert_response :success
  end

  test "should update feature" do
    put :update, id: @feature, feature: { description: @feature.description, name: @feature.name }
    assert_response 204
  end

  test "should destroy feature" do
    assert_difference('Feature.count', -1) do
      delete :destroy, id: @feature
    end

    assert_response 204
  end
end
