require 'test_helper'

class CheckinoutsControllerTest < ActionController::TestCase
  setup do
    @checkinout = checkinouts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:checkinouts)
  end

  test "should create checkinout" do
    assert_difference('Checkinout.count') do
      post :create, checkinout: { checkintime: @checkinout.checkintime, checkouttime: @checkinout.checkouttime, room_id: @checkinout.room_id, user_id: @checkinout.user_id }
    end

    assert_response 201
  end

  test "should show checkinout" do
    get :show, id: @checkinout
    assert_response :success
  end

  test "should update checkinout" do
    put :update, id: @checkinout, checkinout: { checkintime: @checkinout.checkintime, checkouttime: @checkinout.checkouttime, room_id: @checkinout.room_id, user_id: @checkinout.user_id }
    assert_response 204
  end

  test "should destroy checkinout" do
    assert_difference('Checkinout.count', -1) do
      delete :destroy, id: @checkinout
    end

    assert_response 204
  end
end
