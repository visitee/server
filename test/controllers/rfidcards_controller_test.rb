require 'test_helper'

class RfidcardsControllerTest < ActionController::TestCase
  setup do
    @rfidcard = rfidcards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rfidcards)
  end

  test "should create rfidcard" do
    assert_difference('Rfidcard.count') do
      post :create, rfidcard: { description: @rfidcard.description, name: @rfidcard.name, room_id: @rfidcard.room_id }
    end

    assert_response 201
  end

  test "should show rfidcard" do
    get :show, id: @rfidcard
    assert_response :success
  end

  test "should update rfidcard" do
    put :update, id: @rfidcard, rfidcard: { description: @rfidcard.description, name: @rfidcard.name, room_id: @rfidcard.room_id }
    assert_response 204
  end

  test "should destroy rfidcard" do
    assert_difference('Rfidcard.count', -1) do
      delete :destroy, id: @rfidcard
    end

    assert_response 204
  end
end
