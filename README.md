# README #

This README would normally document whatever steps are necessary to get your application 
up and running.

***Note:*** Do **NOT** push to branch **master**. Avoid unnecessary issues (please).

----------------------------------------
#FAQ

[Git Tutorial: 10 Common Git Problems and How to Fix](https://www.codementor.io/git/tutorial/10-common-git-problems-fix)

[Learn Enough Git to be Dangerous](http://www.learnenough.com/git-tutorial)

To be more specific:

1, [Your branch is ahead of 'origin/master' error](http://stackoverflow.com/questions/16288176/your-branch-is-ahead-of-origin-master-by-3-commits)

2, [Made a mistake, how to reset all changes?](http://stackoverflow.com/a/22621464)

3, ...

----------------------------------------
##Getting started

###Starting from scratch (DON'T DO THIS)
Normal projects:
``` 
#!bash
$ mkdir <project_name>
```
Rails projects:
```
#!bash
$ rails new <project_name>
```
Then:
```
#!bash
$ cd /path/to/your/project
$ git init
$ git remote add origin https://<your_username>@bitbucket.org/visitee/<repo_name>.git
```
example:
```
#!bash
$ git remote add origin https://toanndse02449@bitbucket.org/visitee/webapplication.git
```

###Cloning (DO THIS)
```
#!bash
$ git clone https://<your_username>@bitbucket.org/visitee/<repo_name>.git
```
example:
```
#!bash
$ git clone https://toanndse02449@bitbucket.org/visitee/webapplication.git
```

----------------------------------------
##Getting latest update
``` 
#!bash
$ cd /path/to/your/project
$ git checkout master
$ git pull
```

##Branching - Avoid conflicts

### Create and switch to a new branch ###
```
#!zsh
$ cd /path/to/your/project
$ git checkout -b <your_branch_name>
```
If your branch has already existed, just ignore the `-b`, like this:
```
$ git checkout <your_branch_name>
```
***Note:*** Branch name should be: **yourname_yourfeature**

### Push to a new branch ###
After adding and writing your commit message, run the command:
```
$ cd /path/to/your/project
$ git push origin <your_branch_name>
```

### Delete branch ###
Somehow you fucked up and want to start your branch again, delete your (unmerged) branch using the `-D` option:
```
#!zsh
$ git branch -D <your_branch_name>
```

If you have merged your branch with `master` then it's not necessary anymore, delete that branch using the `-d` option:
```
#!zsh
$ git branch -d <your_branch_name>
```

----------------------------------------
##Merging 
After pushing your changes to the repository, you may want to merge it with the **master** branch
###Step 1: Create a Pull Request
![01.png](https://bitbucket.org/repo/zdxdxq/images/2645870724-01.png)

###Step 2: Fill in the details
![aaa.png](https://bitbucket.org/repo/zdxdxq/images/587020356-aaa.png)

###Step 3: Finish it
Click **Create Pull Request** then take a rest, and wait.

After carefully reviewing your changes, administrator will choose to ***Approve*** your ***Decline*** your work, so if bad things happen, blame it on him.

Easy?